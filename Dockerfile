FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive

ENV SPLUNK_PRODUCT splunk
ENV SPLUNK_VERSION 6.5.0
ENV SPLUNK_BUILD 59c8927def0f
ENV SPLUNK_FILENAME splunk-${SPLUNK_VERSION}-${SPLUNK_BUILD}-Linux-x86_64.tgz

ENV SPLUNK_HOME /opt/splunk
ENV SPLUNK_GROUP splunk
ENV SPLUNK_USER splunk
ENV SPLUNK_BACKUP_DEFAULT_ETC /var/opt/splunk

# add splunk:splunk user
RUN adduser --system --group --home ${SPLUNK_HOME} --shell /bin/bash splunk

# make the "en_US.UTF-8" locale so splunk will be utf-8 enabled by default
RUN apt-get update -qqy && \
    apt-get install -qqy --no-install-recommends \
    locales && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# depends
RUN apt-get install -qqy --no-install-recommends \
    libgssapi-krb5-2 \
    wget \
    sudo \
    supervisor

# Download official Splunk release, verify checksum and unzip in /opt/splunk
# Also backup etc folder, so it will be later copied to the linked volume
RUN mkdir -p ${SPLUNK_HOME} \
    && wget --no-check-certificate -qO /tmp/${SPLUNK_FILENAME} https://download.splunk.com/products/${SPLUNK_PRODUCT}/releases/${SPLUNK_VERSION}/linux/${SPLUNK_FILENAME} \
    && wget --no-check-certificate -qO /tmp/${SPLUNK_FILENAME}.md5 https://download.splunk.com/products/${SPLUNK_PRODUCT}/releases/${SPLUNK_VERSION}/linux/${SPLUNK_FILENAME}.md5 \
    && (cd /tmp && md5sum -c ${SPLUNK_FILENAME}.md5) \
    && tar xzf /tmp/${SPLUNK_FILENAME} --strip 1 -C ${SPLUNK_HOME} \
    && rm /tmp/${SPLUNK_FILENAME} \
    && rm /tmp/${SPLUNK_FILENAME}.md5 \
    && mkdir -p /var/opt/splunk \
    && cp -R ${SPLUNK_HOME}/etc ${SPLUNK_BACKUP_DEFAULT_ETC} \
    && rm -fR ${SPLUNK_HOME}/etc \
    && chown -R ${SPLUNK_USER}:${SPLUNK_GROUP} ${SPLUNK_HOME} \
    && chown -R ${SPLUNK_USER}:${SPLUNK_GROUP} ${SPLUNK_BACKUP_DEFAULT_ETC} \
    && rm -rf /var/lib/apt/lists/*

# As this splunk is running behind nginx we need to config it to run so
#RUN sed -i '/^root_endpoint/c\root_endpoint = /splunk' /var/opt/splunk/etc/system/default/web.conf
#RUN sed -i '/.*tools.proxy.on.*/c\tools.proxy.on = True' /var/opt/splunk/etc/system/default/web.conf
RUN sed -i '/^SSOMode/c\SSOMode = permissive' /var/opt/splunk/etc/system/default/web.conf
RUN sed -i '/^SSOMode/a trustedIP = 127.0.0.1' /var/opt/splunk/etc/system/default/web.conf
RUN sed -i '/^allowSsoWithoutChangingServerConf/c\allowSsoWithoutChangingServerConf = 1' /var/opt/splunk/etc/system/default/web.conf
#RUN sed -i '/^enableSplunkWebSSL/c\enableSplunkWebSSL = 1' /var/opt/splunk/etc/system/default/web.conf

COPY build/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Copy new license
#COPY ./Splunk_Enterprise_Q3FY17.lic /var/opt/splunk/etc/licenses/download-trial/Splunk_Enterprise_Q3FY17.lic

# Ports Splunk Web, Splunk Daemon, KVStore, Splunk Indexing Port, Network Input, HTTP Event Collector
EXPOSE 8000/tcp 8089/tcp 8191/tcp 9997/tcp 1514 8088/tcp

WORKDIR /opt/splunk

# Configurations folder, var folder for everything (indexes, logs, kvstore)
VOLUME [ "/opt/splunk/etc", "/opt/splunk/var" ]

CMD ["/entrypoint.sh", "start-service"]

ENTRYPOINT ["/entrypoint.sh"]

